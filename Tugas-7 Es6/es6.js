// jawaban soal 1 (arrow function)

golden = () => {
    console.log ( "this is golden!!" )
}
 golden ()

 // jawaban no 2 (object literal)

 literal = (firstName, lastName)=> {
     return {
         firstName,
         lastName,
         fullName (){
             console.log(firstName, lastName)
         }
     }
 }

 literal ("William", "Imoh"). fullName()

 // jawaban no 3 (Destructuring)

 let newObject= {
      firstName: "Harry",
      lastName: "Potter Holt",
      destination: "Hogwarts React Conf",
      occupation: "Deve-wizard Avocado",
      spell: "Vimulus Renderus!!!",

 }
    let firstName = newObject.firstName;
    let lastName = newObject.lastName;
    let destination = newObject.destination;
    let occupation = newObject.occupation;
    let spell = newObject.spell;


    console.log(firstName, lastName, destination, occupation)

    // jawaban no 4 (Array Spreading)

    const west = ["Will", "Chris", "Sam", "Holly"]
const east = [...west, "Gill", "Brian", "Noel", "Maggie"]
console.log(east)

// jawaban no 5 (Templete Literals)

const planet = "earth"
const view = "glass"
var before =
 `Lorem  ${view} dolor sit amet  
consectetur adipiscing elit, 
${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam `
 
console.log(before) 

