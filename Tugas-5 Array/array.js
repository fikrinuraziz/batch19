
 // jawaban soal 1

function range ( startnum, finishnum ) {
    var arr = []
    if(startnum == null || finishnum == null) {
        return -1
    } else if (startnum > finishnum){
        for ( var i= startnum; i >= finishnum; i--) {
            arr.push(i)
     }
    } else {
      for(var i = startnum; i <= finishnum; i++) {
            arr.push(i)
      }
    }
    return arr
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// jawaban soal 2 

function rangeWithStep ( startnum, finishnum, step ) {
     var array = []
     if ( startnum > finishnum ) {
         for ( var i = startnum; i >= finishnum; i -= step ) {
             array.push(i)
         }
     } else{
       for ( var j = startnum; j <= finishnum; j += step ) {
         array.push(j)
         }
     }
    return array
}             
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 
  
// jawban soal 3 

function sum(a, b, c = 1) {
    var x = 0;
    var y = rangeWithStep(a, b, c)

    if (!a) {
        return 0;
    } else if (a && !b ) {
       return a;
        
    } else {
        for (var i = 0; i < y.length; i++) {
            x += y[i]
        }
    }
    return x
}

console.log(sum(1, 10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15, 10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum()) 

console.log('\n')

// jawaban soal no 4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling() {
    for (var i = 0; i < input.length; i++) {
        console.log('Nomor ID: ' + input[i][0])
        console.log('Nama Lengkap: ' + input[i][1])
        console.log('TTL: ' + input[i][2] + ' ' + input[i][3])
        console.log('Hobi: ' + input[i][4])
        console.log('\n')
    }
}
dataHandling()

console.log('\n')

// jawaban soal no 5

function balikKata(kataInput) {
    var kata = ''
    var length = kataInput.length;
    for(length; length >=0; length--) {
        var word = kataInput[0,length];
        if (kata) {
            kata += word;
        } else {
            kata = word;
        }
    }
    return kata
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\n')

// jawaban soal no 6

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2() {
input.splice(1, 2, "Roman Alamsyah Elsharawy", 'Provinsi Bandar Lampung');
input.splice(4, 1, "Pria", "SMA Internasional Metro")
var nama1 = input.slice(1,2);
var nama = nama1.slice(0, 15);
var tanggal = input[3];
var moon = tanggal.split('/');
var bulan = moon[1];
var gabung = moon.join('-');
moon.sort(function(a,b) {
    return b-a; })
switch (bulan) {
    case 01: console.log( 'Januari' ); break;
    case 02: console.log( 'Februari' ); break;
    case 03: console.log( 'Maret' ); break;
    case 04: console.log( 'April' ); break;
    case 05: console.log( 'Mei' ); break;
    case 06: console.log( 'Juni' ); break;
    case 07: console.log( 'Juli' ); break;
    case 08: console.log( 'Agustus' ); break;
    case 09: console.log( 'September' ); break;
    case 10: console.log( 'Oktober' ); break;
    case 11: console.log( 'November' ); break;
    case 12: console.log( 'Desember' ); break;
    default:  console.log( 'Nothing' ); break;
}

        console.log(input);
        console.log
(bulan);
        console.log(moon);
        console.log(gabung);
        console.log(nama);
}
dataHandling2()


function balikString (Javascript){
    var looping ="";
 for ( var i = Javascript.length -1; i >= 0; i--){
     looping = looping + Javascript[i]
 }
 return looping
}
console.log(balikString("abcde"))
 






